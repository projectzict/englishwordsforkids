package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categoryanimal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoryanimal);

        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Animal");


        Button btnanimal = (Button)findViewById(R.id.btnanimal);
        Button btnpig = (Button)findViewById(R.id.btnpig);
        Button btnsheep = (Button)findViewById(R.id.btnsheep);
        Button btngoat = (Button)findViewById(R.id.btngoat);
        Button btncat = (Button)findViewById(R.id.btncat);
        Button btnrabbit = (Button)findViewById(R.id.btnrabbit);
        Button btndog = (Button)findViewById(R.id.btndog);
        Button btncow = (Button)findViewById(R.id.btncow);
        Button btnhorse = (Button)findViewById(R.id.btnhorse);
        Button btnbuffalo = (Button)findViewById(R.id.btnbuffalo);
        Button btndeer = (Button)findViewById(R.id.btndeer);
        Button btnelephant = (Button)findViewById(R.id.btnelephant);
        Button btngiraffe = (Button)findViewById(R.id.btngiraffe);
        Button btnpanda = (Button)findViewById(R.id.btnpanda);
        Button btnbear = (Button)findViewById(R.id.btnbear);
        Button btncamel = (Button)findViewById(R.id.btncamel);
        Button btnfox = (Button)findViewById(R.id.btnfox);
        Button btnlion = (Button)findViewById(R.id.btnlion);
        Button btntiger = (Button)findViewById(R.id.btntiger);
        Button btnmonkey = (Button)findViewById(R.id.btnmonkey);
        Button btnmouse = (Button)findViewById(R.id.btnmouse);
        Button btnbat = (Button)findViewById(R.id.btnbat);
        Button btnduck = (Button)findViewById(R.id.btnduck);
        Button btnhen = (Button)findViewById(R.id.btnhen);
        Button btnbutterfly = (Button)findViewById(R.id.btnbutterfly);
        Button btnbee = (Button)findViewById(R.id.btnbee);
        Button btnspider = (Button)findViewById(R.id.btnspider);
        Button btnant = (Button)findViewById(R.id.btnant);
        Button btnsnail = (Button)findViewById(R.id.btnsnail);
        Button btncrab = (Button)findViewById(R.id.btncrab);
        Button btnseahorse = (Button)findViewById(R.id.btnseahorse);
        Button btnoctopus = (Button)findViewById(R.id.btnoctopus);
        Button btnseal = (Button)findViewById(R.id.btnseal);
        Button btneel = (Button)findViewById(R.id.btneel);
        Button btnwhale = (Button)findViewById(R.id.btnwhale);
        Button btndolphin = (Button)findViewById(R.id.btndolphin);
        Button btnshark = (Button)findViewById(R.id.btnshark);
        Button btnturtle = (Button)findViewById(R.id.btnturtle);
        Button btnfrog = (Button)findViewById(R.id.btnfrog);
        Button btnbird = (Button)findViewById(R.id.btnbird);
        Button btnfish = (Button)findViewById(R.id.btnfish);
        Button btnzebra = (Button)findViewById(R.id.btnzebra);

        btnanimal.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,animal.class));
            }
        });

        btnpig.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,pig.class));
            }
        });

        btnsheep.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,sheep.class));
            }
        });

        btngoat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,goat.class));
            }
        });

        btncat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,cat.class));
            }
        });

        btnrabbit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,rabbit.class));
            }
        });

        btndog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,dog.class));
            }
        });

        btncow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,cow.class));
            }
        });

        btnhorse.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,horse.class));
            }
        });

        btnbuffalo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,buffalo.class));
            }
        });

        btndeer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,deer.class));
            }
        });

        btnelephant.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,elephant.class));
            }
        });

        btngiraffe.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,giraffe.class));
            }
        });

        btnpanda.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,panda.class));
            }
        });

        btnbear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,bear.class));
            }
        });

        btncamel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,camel.class));
            }
        });

        btnfox.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,fox.class));
            }
        });

        btnlion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,lion.class));
            }
        });

        btntiger.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,tiger.class));
            }
        });

        btnmonkey.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,monkey.class));
            }
        });

        btnmouse.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,mouse.class));
            }
        });

        btnbat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,bat.class));
            }
        });

        btnduck.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,duck.class));
            }
        });

        btnhen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,hen.class));
            }
        });

        btnbutterfly.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,butterfly.class));
            }
        });

        btnbee.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,bee.class));
            }
        });

        btnspider.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,spider.class));
            }
        });

        btnant.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,ant.class));
            }
        });

        btnsnail.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,snail.class));
            }
        });

        btncrab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,crab.class));
            }
        });

        btnseahorse.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,seahorse.class));
            }
        });

        btnoctopus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,octopus.class));
            }
        });

        btnseal.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,seal.class));
            }
        });

        btneel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,eel.class));
            }
        });

        btnwhale.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,whale.class));
            }
        });

        btndolphin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,dolphin.class));
            }
        });

        btnshark.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,shark.class));
            }
        });

        btnturtle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,turtle.class));
            }
        });

        btnfrog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,frog.class));
            }
        });

        btnbird.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,bird.class));
            }
        });

        btnfish.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,fish.class));
            }
        });

        btnzebra.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryanimal.this,zebra.class));
            }
        });
    }
}
