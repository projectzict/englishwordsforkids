package com.projectz.englishwordsforkid;

import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categorymonth extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorymonth);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Month");

        final MediaPlayer monthMP = MediaPlayer.create(this,R.raw.month);
        Button btnmonth = (Button)findViewById(R.id.btnmonth);

        final MediaPlayer janMP = MediaPlayer.create(this,R.raw.jan);
        Button btnjan = (Button)findViewById(R.id.btnjan);

        final MediaPlayer febMP = MediaPlayer.create(this,R.raw.feb);
        Button btnfeb = (Button)findViewById(R.id.btnfeb);

        final MediaPlayer marMP = MediaPlayer.create(this,R.raw.mar);
        Button btnmar = (Button)findViewById(R.id.btnmar);

        final MediaPlayer aprilMP = MediaPlayer.create(this,R.raw.april);
        Button btnapril = (Button)findViewById(R.id.btnapril);

        final MediaPlayer mayMP = MediaPlayer.create(this,R.raw.may);
        Button btnmay = (Button)findViewById(R.id.btnmay);

        final MediaPlayer juneMP = MediaPlayer.create(this,R.raw.june);
        Button btnjune = (Button)findViewById(R.id.btnjune);

        final MediaPlayer julyMP = MediaPlayer.create(this,R.raw.july);
        Button btnjuly = (Button)findViewById(R.id.btnjuly);

        final MediaPlayer augMP = MediaPlayer.create(this,R.raw.aug);
        Button btnaug = (Button)findViewById(R.id.btnaug);

        final MediaPlayer sepMP = MediaPlayer.create(this,R.raw.sep);
        Button btnsep = (Button)findViewById(R.id.btnsep);

        final MediaPlayer octMP = MediaPlayer.create(this,R.raw.oct);
        Button btnoct = (Button)findViewById(R.id.btnoct);

        final MediaPlayer novMP = MediaPlayer.create(this,R.raw.nov);
        Button btnnov = (Button)findViewById(R.id.btnnov);

        final MediaPlayer decMP = MediaPlayer.create(this,R.raw.dec);
        Button btndec = (Button)findViewById(R.id.btndec);

        btnmonth.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                monthMP.start();
            }
        });

        btnjan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                janMP.start();
            }
        });

        btnfeb.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                febMP.start();
            }
        });

        btnmar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                marMP.start();
            }
        });

        btnapril.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                aprilMP.start();
            }
        });

        btnmay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mayMP.start();
            }
        });

        btnjune.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                juneMP.start();
            }
        });

        btnjuly.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                julyMP.start();
            }
        });

        btnaug.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                augMP.start();
            }
        });

        btnsep.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sepMP.start();
            }
        });

        btnoct.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                octMP.start();
            }
        });

        btnnov.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                novMP.start();
            }
        });

        btndec.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                decMP.start();
            }
        });

    }
}
