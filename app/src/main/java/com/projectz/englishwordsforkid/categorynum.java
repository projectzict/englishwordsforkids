package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categorynum extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorynum);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Number");


        Button btnzero = (Button)findViewById(R.id.btnzero);
        Button btnone = (Button)findViewById(R.id.btnone);
        Button btntwo = (Button)findViewById(R.id.btntwo);
        Button btnthree = (Button)findViewById(R.id.btnthree);
        Button btnfour = (Button)findViewById(R.id.btnfour);
        Button btnfive = (Button)findViewById(R.id.btnfive);
        Button btnsix = (Button)findViewById(R.id.btnsix);
        Button btnseven = (Button)findViewById(R.id.btnseven);
        Button btneight = (Button)findViewById(R.id.btneight);
        Button btnnine = (Button)findViewById(R.id.btnnine);
        Button btnten = (Button)findViewById(R.id.btnten);

        final MediaPlayer elevenMP = MediaPlayer.create(this,R.raw.eleven);
        Button btneleven = (Button)findViewById(R.id.btneleven);

        final MediaPlayer twelveMP = MediaPlayer.create(this,R.raw.twelve);
        Button btntwelve = (Button)findViewById(R.id.btntwelve);

        final MediaPlayer thirteenMP = MediaPlayer.create(this,R.raw.thirteen);
        Button btnthirteen = (Button)findViewById(R.id.btnthirteen);

        final MediaPlayer fourteenMP = MediaPlayer.create(this,R.raw.fourteen);
        Button btnfourteen = (Button)findViewById(R.id.btnfourteen);

        final MediaPlayer fifteenMP = MediaPlayer.create(this,R.raw.fifteen);
        Button btnfifteen = (Button)findViewById(R.id.btnfifteen);

        final MediaPlayer sixteenMP = MediaPlayer.create(this,R.raw.sixteen);
        Button btnsixteen = (Button)findViewById(R.id.btnsixteen);

        final MediaPlayer seventeenMP = MediaPlayer.create(this,R.raw.seventeen);
        Button btnseventeen = (Button)findViewById(R.id.btnseventeen);

        final MediaPlayer eighteenMP = MediaPlayer.create(this,R.raw.eighteen);
        Button btneighteen = (Button)findViewById(R.id.btneighteen);

        final MediaPlayer nineteenMP = MediaPlayer.create(this,R.raw.nineteen);
        Button btnnineteen = (Button)findViewById(R.id.btnnineteen);

        final MediaPlayer twentyMP = MediaPlayer.create(this,R.raw.twenty);
        Button btntwenty = (Button)findViewById(R.id.btntwenty);


        final MediaPlayer thirtyMP = MediaPlayer.create(this,R.raw.thirty);
        Button btnthirty = (Button)findViewById(R.id.btnthirty);

        final MediaPlayer fourtyMP = MediaPlayer.create(this,R.raw.fourty);
        Button btnfourty = (Button)findViewById(R.id.btnfourty);

        final MediaPlayer fiftyMP = MediaPlayer.create(this,R.raw.fifty);
        Button btnfifty = (Button)findViewById(R.id.btnfifty);

        final MediaPlayer sixtyMP = MediaPlayer.create(this,R.raw.sixty);
        Button btnsixty = (Button)findViewById(R.id.btnsixty);

        final MediaPlayer seventyMP = MediaPlayer.create(this,R.raw.seventy);
        Button btnseventy = (Button)findViewById(R.id.btnseventy);

        final MediaPlayer eightyMP = MediaPlayer.create(this,R.raw.eighty);
        Button btneighty = (Button)findViewById(R.id.btneighty);

        final MediaPlayer ninetyMP = MediaPlayer.create(this,R.raw.ninety);
        Button btnninety = (Button)findViewById(R.id.btnninety);

        final MediaPlayer onehundredMP = MediaPlayer.create(this,R.raw.onehundred);
        Button btnonehundred = (Button)findViewById(R.id.btnonehundred);

        final MediaPlayer onethousandMP = MediaPlayer.create(this,R.raw.onethousand);
        Button btnonethousand = (Button)findViewById(R.id.btnonethousand);

        final MediaPlayer tenthousandMP = MediaPlayer.create(this,R.raw.tenthousand);
        Button btntenthousand = (Button)findViewById(R.id.btntenthousand);

        final MediaPlayer onehundredthousandMP = MediaPlayer.create(this,R.raw.onehundredthousand);
        Button btnonehundredthousand = (Button)findViewById(R.id.btnonehundredthousand);

        final MediaPlayer onemillionMP = MediaPlayer.create(this,R.raw.onemillion);
        Button btnonemillion = (Button)findViewById(R.id.btnonemillion);

        btnzero.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,zero.class));
            }
        });

        btnone.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,one.class));
            }
        });

        btntwo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,two.class));
            }
        });

        btnthree.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,three.class));
            }
        });

        btnfour.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,four.class));
            }
        });

        btnfive.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,five.class));
            }
        });

        btnsix.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,six.class));
            }
        });

        btnseven.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,seven.class));
            }
        });

        btneight.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,eight.class));
            }
        });

        btnnine.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,nine.class));
            }
        });

        btnten.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorynum.this,ten.class));
            }
        });

        btneleven.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                elevenMP.start();
            }
        });

        btntwelve.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                twelveMP.start();
            }
        });

        btnthirteen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               thirteenMP.start();
            }
        });

        btnfourteen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                fourteenMP.start();
            }
        });

        btnfifteen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                fifteenMP.start();
            }
        });

        btnsixteen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sixteenMP.start();
            }
        });

        btnseventeen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                seventeenMP.start();
            }
        });

        btneighteen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                eighteenMP.start();
            }
        });

        btnnineteen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                nineteenMP.start();
            }
        });

        btntwenty.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                twentyMP.start();
            }
        });

        btnthirty.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                thirtyMP.start();
            }
        });

        btnfourty.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                fourtyMP.start();
            }
        });

        btnfifty.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                fiftyMP.start();
            }
        });

        btnsixty.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sixtyMP.start();
            }
        });

        btnseventy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                seventyMP.start();
            }
        });

        btneighty.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                eightyMP.start();
            }
        });

        btnninety.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ninetyMP.start();
            }
        });

        btnonehundred.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onehundredMP.start();
            }
        });

        btnonethousand.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onethousandMP.start();
            }
        });

        btntenthousand.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                tenthousandMP.start();
            }
        });

        btnonehundredthousand.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onehundredthousandMP.start();
            }
        });

        btnonemillion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onemillionMP.start();
            }
        });
    }
}
