package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class resultfruittest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultfruittest);

        TextView resultLabel = (TextView) findViewById(R.id.resultfruit);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscorefruit);

        int scorefruit = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_FRUIT", 0);
        resultLabel.setText(scorefruit+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_FRUIT",Context.MODE_PRIVATE);
        int highScorefruit = settings.getInt("HIGH_SCORE_FRUIT",0);

        if(scorefruit > highScorefruit){
            HighScoreLabel.setText("High Score : " + scorefruit);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_FRUIT", scorefruit);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScorefruit);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
