package com.projectz.englishwordsforkid;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class numbertest extends AppCompatActivity {
    MediaPlayer baby;
    private TextView countLabel;
    private Button questionLabel;
    private MediaPlayer question;
    private Button answerBtn1;
    private Button answerBtn2;
    private Button answerBtn3;
    private Button answerBtn4;
    private TextView scorenumbertest;

    private String rightAnswer;
    private int rightAnswerCountnumber = 0;
    private int quizCount = 1;
    static final private int QUIZ_COUNT = 10;

    ArrayList<ArrayList<Integer>> quizArray = new ArrayList<>();

    Integer quizData[][] = {
            //{"Country","Right Answer","Choice1","choice2","choice3"}
            {R.raw.one},
            {R.raw.two},
            {R.raw.zero},
            {R.raw.three},
            {R.raw.four},
            {R.raw.five},
            {R.raw.six},
            {R.raw.seven},
            {R.raw.eight},
            {R.raw.nine},
            {R.raw.ten},
            {R.raw.eleven},
            {R.raw.twelve},
            {R.raw.thirteen},
            {R.raw.fourteen},
            {R.raw.fifteen},
            {R.raw.sixteen},
            {R.raw.seventeen},
            {R.raw.eighteen},
            {R.raw.nineteen},
            {R.raw.twenty},
            {R.raw.thirty},
            {R.raw.fourty},
            {R.raw.fifty},
            {R.raw.sixty},
            {R.raw.seventy},
            {R.raw.eighty},
            {R.raw.ninety},
            {R.raw.onehundred},
            {R.raw.onethousand},
            {R.raw.tenthousand},
            {R.raw.onehundredthousand},
            {R.raw.onemillion},





    };

    ArrayList<ArrayList<String>> quizArray2 = new ArrayList<>();

    String quizData2[][] = {
            {"one","four","six","eight"},
            {"two","one","six","eight"},
            {"zero","one","fourteen","eight"},
            {"three","twelve","fourteen","eight"},
            {"four","six","eight","ten"},
            {"five","sixteen","twelve","ten"},
            {"six","eight","ten","twelve"},
            {"seven","one","ten","eight"},
            {"eight","six","twelve","fourteen"},
            {"nine","ten","six","fourteen"},
            {"ten","twelve","fourteen","sixteen"},
            {"eleven","twelve","fourteen","one"},
            {"twelve","fourteen","sixteen","onehundred"},
            {"thirteen","six","sixteen","onehundred"},
            {"fourteen","sixteen","fourteen","onethousand"},
            {"fifteen","sixteen","onehundred","fourteen"},
            {"sixteen","onehundred","onethousand","one"},
            {"seventeen","sixteen","six","one"},
            {"eighteen","onehundred","four","one"},
            {"nineteen","six","four","one"},
            {"twenty","onehundred","four","one"},
            {"thirty","twenty","nineteen","one"},
            {"fourty","thirty","nineteen","seventeen"},
            {"fifty","onehundred","nineteen","eighteen"},
            {"sixty","fifty","fourty","eighteen"},
            {"seventy","nineteen","fourty","eighteen"},
            {"eighty","seventy","fifty","sixty"},
            {"ninety","eighty","seventy","sixty"},
            {"onehundred","onethousand","one","four"},
            {"onethousand","one","onehundred","six"},
            {"tenthousand","onethousand","onehundred","six"},
            {"onehundredthousand","ninety","eighty","six"},
            {"onemillion","onehundredthousand","tenthousand","onehundred"},


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbertest);

        baby = MediaPlayer.create(this,R.raw.babyshark2);
        baby.setLooping(true);
        baby.setVolume(0.01f,0.01f);
        baby.start();

        countLabel = (TextView)findViewById(R.id.countLabel);
        scorenumbertest = (TextView)findViewById(R.id.scorenumbertest);
        questionLabel = (Button)findViewById(R.id.questionLabel);
        answerBtn1 = (Button)findViewById(R.id.answerBtn1);
        answerBtn2 = (Button)findViewById(R.id.answerBtn2);
        answerBtn3 = (Button)findViewById(R.id.answerBtn3);
        answerBtn4 = (Button)findViewById(R.id.answerBtn4);

        // Create quizArray from quizData.
        for(int i = 0; i < quizData.length; i++){

            //Prepare array.
            ArrayList<Integer> tmpArray = new ArrayList<>();
            tmpArray.add(quizData[i][0]); //Country

            //add tmpArray to quizArray.
            quizArray.add(tmpArray);
        }
        for(int i = 0; i < quizData2.length; i++){

            ArrayList<String> tmpArray2 = new ArrayList<>();
            tmpArray2.add(quizData2[i][0]); //Right Answer
            tmpArray2.add(quizData2[i][1]); //Choice1
            tmpArray2.add(quizData2[i][2]); //Choice2
            tmpArray2.add(quizData2[i][3]); //Choice4

            quizArray2.add(tmpArray2);
        }

        showNextQuiz();
    }

    public void showNextQuiz(){

        //Update quizCountLabel.
        countLabel.setText("Q" + quizCount);
        scorenumbertest.setText(rightAnswerCountnumber + " คะแนน");

        Random random = new Random();
        final int randomNum;
        randomNum = random.nextInt(quizArray.size());


        final ArrayList<Integer> quiz = quizArray.get(randomNum);
        question = MediaPlayer.create(this, quiz.get(0));
        questionLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                question.start();
            }
        });
        quiz.remove(0);
        Collections.shuffle(quiz);
        quizArray.remove(randomNum);

        ArrayList<String> answers = quizArray2.get(randomNum);
        rightAnswer = answers.get(0);
        Collections.shuffle(answers);

        answerBtn1.setText(answers.get(0));
        answerBtn2.setText(answers.get(1));
        answerBtn3.setText(answers.get(2));
        answerBtn4.setText(answers.get(3));

        quizArray2.remove(randomNum);
    }

    public void checkAnswer(View view){

        //Get pushed button
        Button answerBtn = (Button) findViewById(view.getId());
        String btnText = answerBtn.getText().toString();

        String alertTitle;

        if (btnText.equals(rightAnswer)){
            //Correct
            alertTitle = "Correct";
            rightAnswerCountnumber++;
            MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.soundcorrect);
            mediaPlayer.start();
        }else{
            //Wrong
            alertTitle = "Wrong";
            MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.soundwrong1);
            mediaPlayer.start();
        }

        //Create Dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(alertTitle);
        builder.setMessage("Answer : " + rightAnswer);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(quizCount == QUIZ_COUNT){
                    // Show Result
                    Intent intent = new Intent(getApplicationContext(),resultnumbertest.class);
                    intent.putExtra("RIGHT_ANSWER_COUNT_NUMBER", rightAnswerCountnumber);
                    startActivity(intent);
                    baby.stop();
                    baby.release();
                }else{
                    quizCount++;
                    showNextQuiz();
                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    public void Quit (View view){
        Intent itn = new Intent(getApplicationContext(), resultnumbertest.class);
        itn.putExtra("RIGHT_ANSWER_COUNT_NUMBER", rightAnswerCountnumber);
        startActivity(itn);
        baby.stop();
        baby.release();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (baby != null) {
            baby.release();
        }
    }
}
