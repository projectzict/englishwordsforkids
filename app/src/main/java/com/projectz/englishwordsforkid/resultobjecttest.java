package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class resultobjecttest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultobjecttest);

        TextView resultLabel = (TextView) findViewById(R.id.resultobject);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscoreobject);

        int scoreobject = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_OBJECT", 0);
        resultLabel.setText(scoreobject+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_OBJECT",Context.MODE_PRIVATE);
        int highScoreobject = settings.getInt("HIGH_SCORE_OBJECT",0);

        if(scoreobject > highScoreobject){
            HighScoreLabel.setText("High Score : " + scoreobject);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_OBJECT", scoreobject);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScoreobject);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
