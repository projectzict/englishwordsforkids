package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categoryfruit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoryfruit);

        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Fruit");

        Button btnapple = (Button)findViewById(R.id.btnapple);
        Button btnorange = (Button)findViewById(R.id.btnorange);
        Button btnmango = (Button)findViewById(R.id.btnmango);
        Button btnpapaya = (Button)findViewById(R.id.btnpapaya);
        Button btndurian = (Button)findViewById(R.id.btndurian);
        Button btnbanana = (Button)findViewById(R.id.btnbanana);
        Button btncoconut = (Button)findViewById(R.id.btncoconut);
        Button btngrape = (Button)findViewById(R.id.btngrape);
        Button btnlychee = (Button)findViewById(R.id.btnlychee);
        Button btnwatermelon = (Button)findViewById(R.id.btnwatermelon);
        Button btnpomelo = (Button)findViewById(R.id.btnpomelo);
        Button btnpineapple = (Button)findViewById(R.id.btnpineapple);


        btnapple.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,apple.class));
            }
        });

        btnorange.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,fruitorange.class));
            }
        });

        btnmango.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,mango.class));
            }
        });

        btnpapaya.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,papaya.class));
            }
        });

        btndurian.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,durian.class));
            }
        });

        btnbanana.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,banana.class));
            }
        });

        btncoconut.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,coconut.class));
            }
        });

        btngrape.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,grape.class));
            }
        });

        btnlychee.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,lychee.class));
            }
        });

        btnwatermelon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,watermelon.class));
            }
        });

        btnpomelo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,pomelo.class));
            }
        });

        btnpineapple.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryfruit.this,pineapple.class));
            }
        });
    }
}
