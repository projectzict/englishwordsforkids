package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.view.Menu;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btncategory = (Button) findViewById(R.id.btncategory);
        Button btntest = (Button) findViewById(R.id.btntest);
        Button btnscore = (Button) findViewById(R.id.buttonscore);

        btncategory.setOnClickListener(this);
        btntest.setOnClickListener(this);
        btnscore.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btncategory:
                Intent itn = new Intent(MainActivity.this, category.class);
                startActivity(itn);
                break;
            case R.id.btntest:
                Intent itn2 = new Intent(MainActivity.this, categorytest.class);
                startActivity(itn2);
                break;
            case R.id.buttonscore:
                Intent itn3 = new Intent(MainActivity.this, score.class);
                startActivity(itn3);
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.Information){
            Intent intent=new Intent(MainActivity.this,credit.class);
            startActivity(intent);
            return true;
        }
        return true;
    }

}
