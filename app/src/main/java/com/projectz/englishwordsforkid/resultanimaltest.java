package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class resultanimaltest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultanimaltest);

        TextView resultLabel = (TextView) findViewById(R.id.resultanimal);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscoreday);

        int scoreanimal = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_ANIMAL", 0);
        resultLabel.setText(scoreanimal+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_ANIMAL",Context.MODE_PRIVATE);
        int highScoreanimal = settings.getInt("HIGH_SCORE",0);

        if(scoreanimal > highScoreanimal){
            HighScoreLabel.setText("High Score : " + scoreanimal);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_ANIMAL", scoreanimal);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScoreanimal);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
