package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categoryvehicle extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoryvehicle);

        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Vehicle");

        Button btnvehicle = (Button)findViewById(R.id.btnvehicle);
        Button btnairplane = (Button)findViewById(R.id.btnairplane);
        Button btnbicycle = (Button)findViewById(R.id.btnbicycle);
        Button btnboat = (Button)findViewById(R.id.btnboat);
        Button btnbus = (Button)findViewById(R.id.btnbus);
        Button btncar = (Button)findViewById(R.id.btncar);
        Button btntrain = (Button)findViewById(R.id.btntrain);
        Button btntruck = (Button)findViewById(R.id.btntruck);
        Button btnmotorcycle = (Button)findViewById(R.id.btnmotorcycle);
        Button btnhelicopter = (Button)findViewById(R.id.btnhelicopter);
        Button btntricycle = (Button)findViewById(R.id.btntricycle);

        btnvehicle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,vehicle.class));
            }
        });

        btnairplane.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,airplane.class));
            }
        });

        btnbicycle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,bicycle.class));
            }
        });

        btnboat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,boat.class));
            }
        });

        btnbus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,bus.class));
            }
        });

        btncar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,car.class));
            }
        });

        btntrain.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,train.class));
            }
        });

        btntruck.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,truck.class));
            }
        });

        btnmotorcycle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,motorcycle.class));
            }
        });

        btnhelicopter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,helicopter.class));
            }
        });

        btntricycle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryvehicle.this,tricycle.class));
            }
        });

    }
}
