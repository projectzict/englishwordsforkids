package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categoryabc extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoryabc);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("ABC");

        final MediaPlayer aMP = MediaPlayer.create(this,R.raw.a);
        Button btna = (Button)findViewById(R.id.btna);

        final MediaPlayer bMP = MediaPlayer.create(this,R.raw.b);
        Button btnb = (Button)findViewById(R.id.btnb);

        final MediaPlayer cMP = MediaPlayer.create(this,R.raw.c);
        Button btnc = (Button)findViewById(R.id.btnc);

        final MediaPlayer dMP = MediaPlayer.create(this,R.raw.d);
        Button btnd = (Button)findViewById(R.id.btnd);

        final MediaPlayer eMP = MediaPlayer.create(this,R.raw.e);
        Button btne = (Button)findViewById(R.id.btne);

        final MediaPlayer fMP = MediaPlayer.create(this,R.raw.f);
        Button btnf = (Button)findViewById(R.id.btnf);

        final MediaPlayer gMP = MediaPlayer.create(this,R.raw.g);
        Button btng = (Button)findViewById(R.id.btng);

        final MediaPlayer hMP = MediaPlayer.create(this,R.raw.h);
        Button btnh = (Button)findViewById(R.id.btnh);

        final MediaPlayer iMP = MediaPlayer.create(this,R.raw.i);
        Button btni = (Button)findViewById(R.id.btni);

        final MediaPlayer jMP = MediaPlayer.create(this,R.raw.j);
        Button btnj = (Button)findViewById(R.id.btnj);

        final MediaPlayer kMP = MediaPlayer.create(this,R.raw.k);
        Button btnk = (Button)findViewById(R.id.btnk);

        final MediaPlayer lMP = MediaPlayer.create(this,R.raw.l);
        Button btnl = (Button)findViewById(R.id.btnl);

        final MediaPlayer mMP = MediaPlayer.create(this,R.raw.m);
        Button btnm = (Button)findViewById(R.id.btnm);

        final MediaPlayer nMP = MediaPlayer.create(this,R.raw.n);
        Button btnn = (Button)findViewById(R.id.btnn);

        final MediaPlayer oMP = MediaPlayer.create(this,R.raw.o);
        Button btno = (Button)findViewById(R.id.btno);

        final MediaPlayer pMP = MediaPlayer.create(this,R.raw.p);
        Button btnp = (Button)findViewById(R.id.btnp);

        final MediaPlayer qMP = MediaPlayer.create(this,R.raw.q);
        Button btnq = (Button)findViewById(R.id.btnq);

        final MediaPlayer rMP = MediaPlayer.create(this,R.raw.r);
        Button btnr = (Button)findViewById(R.id.btnr);

        final MediaPlayer sMP = MediaPlayer.create(this,R.raw.s);
        Button btns = (Button)findViewById(R.id.btns);

        final MediaPlayer tMP = MediaPlayer.create(this,R.raw.t);
        Button btnt = (Button)findViewById(R.id.btnt);

        final MediaPlayer uMP = MediaPlayer.create(this,R.raw.u);
        Button btnu = (Button)findViewById(R.id.btnu);

        final MediaPlayer vMP = MediaPlayer.create(this,R.raw.v);
        Button btnv = (Button)findViewById(R.id.btnv);

        final MediaPlayer wMP = MediaPlayer.create(this,R.raw.w);
        Button btnw = (Button)findViewById(R.id.btnw);

        final MediaPlayer xMP = MediaPlayer.create(this,R.raw.x);
        Button btnx = (Button)findViewById(R.id.btnx);

        final MediaPlayer yMP = MediaPlayer.create(this,R.raw.y);
        Button btny = (Button)findViewById(R.id.btny);

        final MediaPlayer zMP = MediaPlayer.create(this,R.raw.z);
        Button btnz = (Button)findViewById(R.id.btnz);

        btna.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                aMP.start();
            }
        });

        btnb.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                bMP.start();
            }
        });

        btnc.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                cMP.start();
            }
        });

        btnd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dMP.start();
            }
        });

        btne.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                eMP.start();
            }
        });

        btnf.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                fMP.start();
            }
        });

        btng.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                gMP.start();
            }
        });

        btnh.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                hMP.start();
            }
        });

        btni.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                iMP.start();
            }
        });

        btnj.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               jMP.start();
            }
        });

        btnk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                kMP.start();
            }
        });

        btnl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                lMP.start();
            }
        });

        btnm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               mMP.start();
            }
        });

        btnn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                nMP.start();
            }
        });

        btno.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                oMP.start();
            }
        });

        btnp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                pMP.start();
            }
        });

        btnq.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                qMP.start();
            }
        });

        btnr.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               rMP.start();
            }
        });

        btns.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sMP.start();
            }
        });

        btnt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                tMP.start();
            }
        });

        btnu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                uMP.start();
            }
        });

        btnv.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                vMP.start();
            }
        });

        btnw.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                wMP.start();
            }
        });

        btnx.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                xMP.start();
            }
        });

        btny.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                yMP.start();
            }
        });

        btnz.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                zMP.start();
            }
        });

    }
}
