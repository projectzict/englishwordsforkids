package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class resultdaytest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultdaytest);

        TextView resultLabel = (TextView) findViewById(R.id.resultday);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscoreday);

        int scoreday = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_DAY", 0);
        resultLabel.setText(scoreday+" / 7");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_DAY",Context.MODE_PRIVATE);
        int highScoreday = settings.getInt("HIGH_SCORE_DAY",0);

        if(scoreday > highScoreday){
            HighScoreLabel.setText("High Score : " + scoreday);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_DAY", scoreday);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScoreday);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
