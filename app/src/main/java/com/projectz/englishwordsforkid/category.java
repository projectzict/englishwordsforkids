package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class category extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Category");

        Button btnday = (Button)findViewById(R.id.btnday);
        Button btnmonth = (Button)findViewById(R.id.btnmonth);
        Button btnabc = (Button)findViewById(R.id.btnabc);
        Button btnnum = (Button)findViewById(R.id.btnnum);
        Button btncolor = (Button)findViewById(R.id.btncolor);
        Button btnbody = (Button)findViewById(R.id.btnbody);
        Button btnanimal = (Button)findViewById(R.id.btnanimal);
        Button btnfruit = (Button)findViewById(R.id.btnfruit);
        Button btnobject = (Button)findViewById(R.id.btnobject);
        Button btnverb = (Button)findViewById(R.id.btnverb);


        btnday.setOnClickListener(this);
        btnmonth.setOnClickListener(this);
        btnabc.setOnClickListener(this);
        btnnum.setOnClickListener(this);
        btncolor.setOnClickListener(this);
        btnbody.setOnClickListener(this);
        btnanimal.setOnClickListener(this);
        btnfruit.setOnClickListener(this);
        btnobject.setOnClickListener(this);
        btnverb.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnday:
                Intent itn = new Intent(category.this,categoryday.class);
                startActivity(itn);
                break;
            case R.id.btnmonth:
                Intent itn2 = new Intent(category.this,categorymonth.class);
                startActivity(itn2);
                break;
            case R.id.btnabc:
                Intent itn3 = new Intent(category.this,categoryabc.class);
                startActivity(itn3);
                break;
            case R.id.btnnum:
                Intent itn4 = new Intent(category.this,categorynum.class);
                startActivity(itn4);
                break;
            case R.id.btncolor:
                Intent itn5 = new Intent(category.this,categorycolor.class);
                startActivity(itn5);
                break;
            case R.id.btnbody:
                Intent itn6 = new Intent(category.this,categorybody.class);
                startActivity(itn6);
                break;
            case R.id.btnanimal:
                Intent itn7 = new Intent(category.this,categoryanimal.class);
                startActivity(itn7);
                break;
            case R.id.btnfruit:
                Intent itn8 = new Intent(category.this,categoryfruit.class);
                startActivity(itn8);
                break;
            case R.id.btnobject:
                Intent itn9 = new Intent(category.this,categoryobject.class);
                startActivity(itn9);
                break;
            case R.id.btnverb:
                Intent itn10 = new Intent(category.this,categoryvehicle.class);
                startActivity(itn10);
                break;
        }

    }


}

