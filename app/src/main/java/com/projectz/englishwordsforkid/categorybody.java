package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categorybody extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorybody);

        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Body");

        Button btneye = (Button)findViewById(R.id.btneye);
        Button btnear = (Button)findViewById(R.id.btnear);
        Button btnnose = (Button)findViewById(R.id.btnnose);
        Button btncheek = (Button)findViewById(R.id.btncheek);
        Button btnmouth = (Button)findViewById(R.id.btnmouth);
        Button btnlip = (Button)findViewById(R.id.btnlip);
        Button btnchin = (Button)findViewById(R.id.btnchin);
        Button btnneck = (Button)findViewById(R.id.btnneck);
        Button btntooth = (Button)findViewById(R.id.btntooth);
        Button btntongue = (Button)findViewById(R.id.btntongue);
        Button btnarm = (Button)findViewById(R.id.btnarm);
        Button btnfinger = (Button)findViewById(R.id.btnfinger);
        Button btnknee = (Button)findViewById(R.id.btnknee);
        Button btnleg = (Button)findViewById(R.id.btnleg);
        Button btnhair = (Button)findViewById(R.id.btnhair);
        Button btnhead = (Button)findViewById(R.id.btnhead);

        btneye.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,eye.class));
            }
        });

        btnear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,ear.class));
            }
        });

        btnnose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,nose.class));
            }
        });

        btncheek.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,cheek.class));
            }
        });

        btnmouth.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,mouth.class));
            }
        });

        btnlip.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,lip.class));
            }
        });

        btnchin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,chin.class));
            }
        });

        btnneck.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,neck.class));
            }
        });

        btntooth.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,tooth.class));
            }
        });

        btntongue.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,tongue.class));
            }
        });

        btnarm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,arm.class));
            }
        });

        btnfinger.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,finger.class));
            }
        });

        btnknee.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,knee.class));
            }
        });

        btnleg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,leg.class));
            }
        });

        btnhair.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,hair.class));
            }
        });

        btnhead.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorybody.this,head.class));
            }
        });
    }
}
