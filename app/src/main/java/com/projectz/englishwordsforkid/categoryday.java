package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categoryday extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoryday);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Day");

        final MediaPlayer sundayMP = MediaPlayer.create(this,R.raw.sunday);
        Button btnsun = (Button)findViewById(R.id.btnsunday);

        final MediaPlayer mondayMP = MediaPlayer.create(this,R.raw.monday);
        Button btnmon = (Button)findViewById(R.id.btnmonday);

        final MediaPlayer tuesdayMP = MediaPlayer.create(this,R.raw.tuesday);
        Button btntues = (Button)findViewById(R.id.btntuesday);

        final MediaPlayer wednesdayMP = MediaPlayer.create(this,R.raw.wednesday);
        Button btnwed = (Button)findViewById(R.id.btnwednesday);

        final MediaPlayer thursdayMP = MediaPlayer.create(this,R.raw.thursday);
        Button btnthur = (Button)findViewById(R.id.btnthursday);

        final MediaPlayer fridayMP = MediaPlayer.create(this,R.raw.friday);
        Button btnfri = (Button)findViewById(R.id.btnfriday);

        final MediaPlayer saturdayMP = MediaPlayer.create(this,R.raw.saturday);
        Button btnsat = (Button)findViewById(R.id.btnsaturday);

        btnsun.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sundayMP.start();
            }
        });

        btnmon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mondayMP.start();

            }
        });

        btntues.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                tuesdayMP.start();

            }
        });

        btnwed.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                wednesdayMP.start();

            }
        });

        btnthur.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                thursdayMP.start();

            }
        });

        btnfri.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                fridayMP.start();

            }
        });

        btnsat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                saturdayMP.start();
            }
        });
    }

}
