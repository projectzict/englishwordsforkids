package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categorytest extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorytest);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Category Test");

        Button btndaytest = (Button)findViewById(R.id.btndaytest);
        Button btnmonthtest = (Button)findViewById(R.id.btnmonthtest);
        Button btnabctest = (Button)findViewById(R.id.btnabctest);
        Button btnnumtest = (Button)findViewById(R.id.btnnumtest);
        Button btncolortest = (Button)findViewById(R.id.btncolortest);
        Button btnbodytest = (Button)findViewById(R.id.btnbodytest);
        Button btnanimaltest = (Button)findViewById(R.id.btnanimaltest);
        Button btnfruittest = (Button)findViewById(R.id.btnfruittest);
        Button btnobjecttest = (Button)findViewById(R.id.btnobjecttest);
        Button btnverbtest = (Button)findViewById(R.id.btnverbtest);


        btndaytest.setOnClickListener(this);
        btnmonthtest.setOnClickListener(this);
        btnabctest.setOnClickListener(this);
        btnnumtest.setOnClickListener(this);
        btncolortest.setOnClickListener(this);
        btnbodytest.setOnClickListener(this);
        btnanimaltest.setOnClickListener(this);
        btnfruittest.setOnClickListener(this);
        btnobjecttest.setOnClickListener(this);
        btnverbtest.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btndaytest:
                Intent itn = new Intent(categorytest.this,daytest.class);
                startActivity(itn);
                break;
            case R.id.btnmonthtest:
                Intent itn2 = new Intent(categorytest.this,monthtest.class);
                startActivity(itn2);
                break;
            case R.id.btnabctest:
                Intent itn3 = new Intent(categorytest.this,abctest.class);
                startActivity(itn3);
                break;
            case R.id.btnnumtest:
                Intent itn4 = new Intent(categorytest.this,numbertest.class);
                startActivity(itn4);
                break;
            case R.id.btncolortest:
                Intent itn5 = new Intent(categorytest.this,colortest.class);
                startActivity(itn5);
                break;
            case R.id.btnbodytest:
                Intent itn6 = new Intent(categorytest.this,bodytest.class);
                startActivity(itn6);
                break;
            case R.id.btnanimaltest:
                Intent itn7 = new Intent(categorytest.this,animaltest.class);
                startActivity(itn7);
                break;
            case R.id.btnfruittest:
                Intent itn8 = new Intent(categorytest.this,fruittest.class);
                startActivity(itn8);
                break;
            case R.id.btnobjecttest:
                Intent itn9 = new Intent(categorytest.this,objecttest.class);
                startActivity(itn9);
                break;
            case R.id.btnverbtest:
                Intent itn10 = new Intent(categorytest.this,vehicletest.class);
                startActivity(itn10);
                break;
        }

    }


}