package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class resultvehicletest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultvehicletest);

        TextView resultLabel = (TextView) findViewById(R.id.resultvehicle);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscorevehicle);

        int scorevehicle = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_VEHICLE", 0);
        resultLabel.setText(scorevehicle+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_VEHICLE",Context.MODE_PRIVATE);
        int highScorevehicle = settings.getInt("HIGH_SCORE_VEHICLE",0);

        if(scorevehicle > highScorevehicle){
            HighScoreLabel.setText("High Score : " + scorevehicle);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_VEHICLE", scorevehicle);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScorevehicle);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
