package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class animaltest extends AppCompatActivity {
    MediaPlayer baby;
    private TextView countLabel;
    private TextView scoreanimaltest;
    private ImageView questionLabel;
    private Button answerBtn1;
    private Button answerBtn2;
    private Button answerBtn3;
    private Button answerBtn4;

    private String rightAnswer;
    private int rightAnswerCountanimal = 0;
    private int quizCount = 1;
    static final private int QUIZ_COUNT = 10;

    ArrayList<ArrayList<Integer>> quizArray = new ArrayList<>();

    Integer quizData[][] = {
            //{"Country","Right Answer","Choice1","choice2","choice3"}
            {R.drawable.quizant},
            {R.drawable.quizbat},
            {R.drawable.quizbear},
            {R.drawable.quizbee},
            {R.drawable.quizbird},
            {R.drawable.quizbuffalo},
            {R.drawable.quizbutterfly},
            {R.drawable.quizcamel},
            {R.drawable.quizcat},
            {R.drawable.quizcow},
            {R.drawable.quizcrab},
            {R.drawable.quizdeer},
            {R.drawable.quizdog},
            {R.drawable.quizdolphin},
            {R.drawable.quizduck},
            {R.drawable.quizeel},
            {R.drawable.quizelephant},
            {R.drawable.quizfish},
            {R.drawable.quizfrog},
            {R.drawable.quizfox},
            {R.drawable.quizgiraffe},
            {R.drawable.quizgoat},
            {R.drawable.quizhen},
            {R.drawable.quizhorse},
            {R.drawable.quizlion},
            {R.drawable.quizmonkey},
            {R.drawable.quizmouse},
            {R.drawable.quizoctopus},
            {R.drawable.quizpanda},
            {R.drawable.quizpig},
            {R.drawable.quizrabbit},
            {R.drawable.quizseahorse},
            {R.drawable.quizseal},
            {R.drawable.quizshark},
            {R.drawable.quizsheep},
            {R.drawable.quizsnail},
            {R.drawable.quizspider},
            {R.drawable.quiztiger},
            {R.drawable.quizturtle},
            {R.drawable.quizwhale},
            {R.drawable.quizzebra}
    };

    ArrayList<ArrayList<String>> quizArray2 = new ArrayList<>();

    String quizData2[][] = {
            {"ant","sheep","goat","cat"},
            {"bat","goat","cat","pig"},
            {"bear","pig","cat","sheep"},
            {"bee","sheep","pig","goat"},
            {"bird","pig","sheep","goat"},
            {"buffalo","rabbit","pig","sheep"},
            {"butterfly","dog","rabbit","pig"},
            {"camel","cow","dog","pig"},
            {"cat","pig","dog","cow"},
            {"cow","pig","dog","buffalo"},
            {"crab","deer","octopus","cat"},
            {"deer","goat","eel","pig"},
            {"dog","pig","cat","dolphin"},
            {"dolphin","sheep","pig","goat"},
            {"duck","pig","sheep","hen"},
            {"eel","rabbit","pig","sheep"},
            {"elephant","dog","rabbit","pig"},
            {"fish","cow","dog","pig"},
            {"frog","pig","lion","cow"},
            {"fox","pig","dog","buffalo"},
            {"giraffe","sheep","goat","cat"},
            {"goat","sheep","mouse","lion"},
            {"hen","goat","cat","pig"},
            {"horse","pig","seahorse","cat"},
            {"lion","mouse","pig","tiger"},
            {"monkey","pig","sheep","goat"},
            {"mouse","rabbit","pig","sheep"},
            {"octopus","dog","rabbit","hen"},
            {"panda","cow","dog","monkey"},
            {"pig","giraffe","seal","cow"},
            {"rabbit","pig","dog","buffalo"},
            {"seahorse","cow","panda","pig"},
            {"seal","pig","dog","cow"},
            {"shark","pig","dog","seal"},
            {"sheep","goat","zebra","cat"},
            {"snail","goat","cat","pig"},
            {"spider","pig","rabbit","cat"},
            {"tiger","lion","pig","goat"},
            {"turtle","pig","sheep","tiger"},
            {"whale","rabbit","dolphin","sheep"},
            {"zebra","horse","rabbit","pig"}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animaltest);

        baby = MediaPlayer.create(this,R.raw.babyshark2);
        baby.setLooping(true);
        baby.setVolume(0.01f,0.01f);
        baby.start();

        scoreanimaltest = (TextView)findViewById(R.id.scoreanimaltest);
        countLabel = (TextView)findViewById(R.id.countLabel);
        questionLabel = (ImageView)findViewById(R.id.questionLabel);
        answerBtn1 = (Button)findViewById(R.id.answerBtn1);
        answerBtn2 = (Button)findViewById(R.id.answerBtn2);
        answerBtn3 = (Button)findViewById(R.id.answerBtn3);
        answerBtn4 = (Button)findViewById(R.id.answerBtn4);



        // Create quizArray from quizData.
        for(int i = 0; i < quizData.length; i++){

            //Prepare array.
            ArrayList<Integer> tmpArray = new ArrayList<>();
            tmpArray.add(quizData[i][0]); //Country

            //add tmpArray to quizArray.
            quizArray.add(tmpArray);
        }
        for(int i = 0; i < quizData2.length; i++){

            ArrayList<String> tmpArray2 = new ArrayList<>();
            tmpArray2.add(quizData2[i][0]); //Right Answer
            tmpArray2.add(quizData2[i][1]); //Choice1
            tmpArray2.add(quizData2[i][2]); //Choice2
            tmpArray2.add(quizData2[i][3]); //Choice4

            quizArray2.add(tmpArray2);
        }

        showNextQuiz();
    }

    public void showNextQuiz(){

        //Update quizCountLabel.
        countLabel.setText("Q" + quizCount);
        scoreanimaltest.setText(rightAnswerCountanimal + " คะแนน");

        Random random = new Random();
        int randomNum;
        randomNum = random.nextInt(quizArray.size());


        ArrayList<Integer> quiz = quizArray.get(randomNum);
        questionLabel.setImageResource(quiz.get(0));
        quiz.remove(0);
        Collections.shuffle(quiz);
        quizArray.remove(randomNum);

        ArrayList<String> answers = quizArray2.get(randomNum);
        rightAnswer = answers.get(0);
        Collections.shuffle(answers);

        answerBtn1.setText(answers.get(0));
        answerBtn2.setText(answers.get(1));
        answerBtn3.setText(answers.get(2));
        answerBtn4.setText(answers.get(3));

        quizArray2.remove(randomNum);
    }

    public void checkAnswer(View view){

        //Get pushed button
        Button answerBtn = (Button) findViewById(view.getId());
        String btnText = answerBtn.getText().toString();

        String alertTitle;

        if (btnText.equals(rightAnswer)){
            //Correct
            alertTitle = "Correct";
            rightAnswerCountanimal++;
            MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.soundcorrect);
            mediaPlayer.start();
        }else{
            //Wrong
            alertTitle = "Wrong";
            MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.soundwrong1);
            mediaPlayer.start();
        }

        //Create Dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(alertTitle);
        builder.setMessage("Answer : " + rightAnswer);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(quizCount == QUIZ_COUNT){
                    // Show Result
                    Intent intent = new Intent(getApplicationContext(),resultanimaltest.class);
                    intent.putExtra("RIGHT_ANSWER_COUNT_ANIMAL", rightAnswerCountanimal);
                    startActivity(intent);
                    baby.stop();
                    baby.release();
                }else{
                    quizCount++;
                    showNextQuiz();
                }
            }
        });
        builder.setCancelable(false);
        builder.show();

    }
    public void Quit (View view){
        Intent itn = new Intent(getApplicationContext(), resultanimaltest.class);
        itn.putExtra("RIGHT_ANSWER_COUNT_ANIMAL", rightAnswerCountanimal);
        startActivity(itn);
        baby.stop();
        baby.release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(baby !=null){
            baby.release();
        }
    }
}
