package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categoryobject extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoryobject);

        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Object");

        Button btndoor = (Button)findViewById(R.id.btndoor);
        Button btnwindow = (Button)findViewById(R.id.btnwindow);
        Button btnbed = (Button)findViewById(R.id.btnbed);
        Button btnpen = (Button)findViewById(R.id.btnpen);
        Button btnbag = (Button)findViewById(R.id.btnbag);
        Button btnbook = (Button)findViewById(R.id.btnbook);
        Button btnpencil = (Button)findViewById(R.id.btnpencil);
        Button btnbox = (Button)findViewById(R.id.btnbox);
        Button btntable = (Button)findViewById(R.id.btntable);
        Button btnchair = (Button)findViewById(R.id.btnchair);
        Button btnbin = (Button)findViewById(R.id.btnbin);
        Button btnbroom = (Button)findViewById(R.id.btnbroom);
        Button btnnotebook = (Button)findViewById(R.id.btnnotebook);
        Button btnruler = (Button)findViewById(R.id.btnruler);
        Button btneraser = (Button)findViewById(R.id.btneraser);
        Button btncap = (Button)findViewById(R.id.btncap);
        Button btngun = (Button)findViewById(R.id.btngun);
        Button btnpan = (Button)findViewById(R.id.btnpan);
        Button btnumbrella = (Button)findViewById(R.id.btnumbrella);
        Button btnyoyo = (Button)findViewById(R.id.btnyoyo);
        Button btnball = (Button)findViewById(R.id.btnball);
        Button btnscissors = (Button)findViewById(R.id.btnscissors);
        Button btndoll = (Button)findViewById(R.id.btndoll);
        Button btnhat = (Button)findViewById(R.id.btnhat);
        Button btnkite = (Button)findViewById(R.id.btnkite);
        Button btnstapler = (Button)findViewById(R.id.btnstapler);
        Button btnrobot = (Button)findViewById(R.id.btnrobot);
        Button btnfan = (Button)findViewById(R.id.btnfan);
        Button btntop = (Button)findViewById(R.id.btntop);
        Button btnring = (Button)findViewById(R.id.btnring);
        Button btnbasket = (Button)findViewById(R.id.btnbasket);
        Button btncard = (Button)findViewById(R.id.btncard);
        Button btngift = (Button)findViewById(R.id.btngift);

        btndoor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,door.class));
            }
        });

        btnwindow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,window.class));
            }
        });

        btnbed.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,bed.class));
            }
        });

        btnpen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,pen.class));
            }
        });

        btnbag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,bag.class));
            }
        });

        btnbook.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,book.class));
            }
        });

        btnpencil.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,pencil.class));
            }
        });

        btnbox.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,box.class));
            }
        });

        btntable.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,table.class));
            }
        });

        btnchair.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,chair.class));
            }
        });

        btnbin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,bin.class));
            }
        });

        btnbroom.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,broom.class));
            }
        });

        btnnotebook.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,notebook.class));
            }
        });

        btnruler.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,ruler.class));
            }
        });

        btneraser.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,eraser.class));
            }
        });

        btncap.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,cap.class));
            }
        });

        btngun.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,gun.class));
            }
        });

        btnpan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,pan.class));
            }
        });

        btnumbrella.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,umbrella.class));
            }
        });

        btnyoyo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,yoyo.class));
            }
        });

        btnball.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,ball.class));
            }
        });

        btnscissors.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,scissors.class));
            }
        });

        btndoll.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,doll.class));
            }
        });

        btnhat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,hat.class));
            }
        });

        btnkite.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,kite.class));
            }
        });

        btnstapler.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,stapler.class));
            }
        });

        btnrobot.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,robot.class));
            }
        });

        btnfan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,fan.class));
            }
        });

        btntop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,top.class));
            }
        });

        btnring.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,ring.class));
            }
        });

        btnbasket.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,basket.class));
            }
        });

        btncard.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,card.class));
            }
        });

        btngift.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categoryobject.this,gift.class));
            }
        });

    }
}
