package com.projectz.englishwordsforkid;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class colortest extends AppCompatActivity {
    MediaPlayer baby;
    private TextView countLabel;
    private ImageView questionLabel;
    private Button answerBtn1;
    private Button answerBtn2;
    private Button answerBtn3;
    private Button answerBtn4;
    private TextView scorecolortest;

    private String rightAnswer;
    private int rightAnswerCountcolor = 0;
    private int quizCount = 1;
    static final private int QUIZ_COUNT = 10;

    ArrayList<ArrayList<Integer>> quizArray = new ArrayList<>();

    Integer quizData[][] = {
            //{"Country","Right Answer","Choice1","choice2","choice3"}
            {R.drawable.whitetest},
            {R.drawable.purpletest},
            {R.drawable.bluetest},
            {R.drawable.greentest},
            {R.drawable.yellowtest},
            {R.drawable.orangetest},
            {R.drawable.redtest},
            {R.drawable.pinktest},
            {R.drawable.browntest},
            {R.drawable.blacktest},
    };

    ArrayList<ArrayList<String>> quizArray2 = new ArrayList<>();

    String quizData2[][] = {
            {"white","purple","blue","green"},
            {"purple","blue","green","yellow"},
            {"blue","green","yellow","orange"},
            {"green","yellow","orange","red"},
            {"yellow","orange","red","pink"},
            {"orange","red","pink","brown"},
            {"red","pink","brown","black"},
            {"pink","brown","black","white"},
            {"brown","black","white","purple"},
            {"black","white","purple","blue"}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colortest);

        baby = MediaPlayer.create(this,R.raw.babyshark2);
        baby.setLooping(true);
        baby.setVolume(0.01f,0.01f);
        baby.start();

        countLabel = (TextView)findViewById(R.id.countLabel);
        scorecolortest = (TextView) findViewById(R.id.scorecolortest);
        questionLabel = (ImageView)findViewById(R.id.questionLabel);
        answerBtn1 = (Button)findViewById(R.id.answerBtn1);
        answerBtn2 = (Button)findViewById(R.id.answerBtn2);
        answerBtn3 = (Button)findViewById(R.id.answerBtn3);
        answerBtn4 = (Button)findViewById(R.id.answerBtn4);

        // Create quizArray from quizData.
        for(int i = 0; i < quizData.length; i++){

            //Prepare array.
            ArrayList<Integer> tmpArray = new ArrayList<>();
            tmpArray.add(quizData[i][0]); //Country

            //add tmpArray to quizArray.
            quizArray.add(tmpArray);
        }
        for(int i = 0; i < quizData2.length; i++){

            ArrayList<String> tmpArray2 = new ArrayList<>();
            tmpArray2.add(quizData2[i][0]); //Right Answer
            tmpArray2.add(quizData2[i][1]); //Choice1
            tmpArray2.add(quizData2[i][2]); //Choice2
            tmpArray2.add(quizData2[i][3]); //Choice4

            quizArray2.add(tmpArray2);
        }

        showNextQuiz();
    }

    public void showNextQuiz(){

        //Update quizCountLabel.
        countLabel.setText("Q" + quizCount);
        scorecolortest.setText(rightAnswerCountcolor + " คะแนน");

        Random random = new Random();
        int randomNum;
        randomNum = random.nextInt(quizArray.size());


        ArrayList<Integer> quiz = quizArray.get(randomNum);
        questionLabel.setImageResource(quiz.get(0));
        quiz.remove(0);
        Collections.shuffle(quiz);
        quizArray.remove(randomNum);

        ArrayList<String> answers = quizArray2.get(randomNum);
        rightAnswer = answers.get(0);
        Collections.shuffle(answers);

        answerBtn1.setText(answers.get(0));
        answerBtn2.setText(answers.get(1));
        answerBtn3.setText(answers.get(2));
        answerBtn4.setText(answers.get(3));

        quizArray2.remove(randomNum);
    }

    public void checkAnswer(View view){

        //Get pushed button
        Button answerBtn = (Button) findViewById(view.getId());
        String btnText = answerBtn.getText().toString();

        String alertTitle;

        if (btnText.equals(rightAnswer)){
            //Correct
            alertTitle = "Correct";
            rightAnswerCountcolor++;
            MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.soundcorrect);
            mediaPlayer.start();
        }else{
            //Wrong
            alertTitle = "Wrong";
            MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.soundwrong1);
            mediaPlayer.start();
        }

        //Create Dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(alertTitle);
        builder.setMessage("Answer : " + rightAnswer);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(quizCount == QUIZ_COUNT){
                    // Show Result
                    Intent intent = new Intent(getApplicationContext(),resultcolortest.class);
                    intent.putExtra("RIGHT_ANSWER_COUNT_COLOR", rightAnswerCountcolor);
                    startActivity(intent);
                    baby.stop();
                    baby.release();
                }else{
                    quizCount++;
                    showNextQuiz();
                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }
    public void Quit (View view){
        Intent itn = new Intent(getApplicationContext(), resultcolortest.class);
        itn.putExtra("RIGHT_ANSWER_COUNT_COLOR", rightAnswerCountcolor);
        startActivity(itn);
        baby.stop();
        baby.release();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (baby != null) {
            baby.release();
        }
    }
}
