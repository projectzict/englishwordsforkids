package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class resultmonthtest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultmonthtest);

        TextView resultLabel = (TextView) findViewById(R.id.resultday);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscoreday);

        int scoremonth = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_MONTH", 0);
        resultLabel.setText(scoremonth+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_MONTH",Context.MODE_PRIVATE);
        int highScoreMonth = settings.getInt("HIGH_SCORE_MONTH",0);

        if(scoremonth > highScoreMonth){
            HighScoreLabel.setText("High Score : " + scoremonth);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_MONTH", scoremonth);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScoreMonth);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
