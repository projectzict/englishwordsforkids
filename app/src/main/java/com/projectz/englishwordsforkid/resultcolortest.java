package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class resultcolortest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultcolortest);

        TextView resultLabel = (TextView) findViewById(R.id.resultanimal);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscoreday);

        int scorecolor = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_COLOR", 0);
        resultLabel.setText(scorecolor+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_COLOR",Context.MODE_PRIVATE);
        int highScorecolor = settings.getInt("HIGH_SCORE_COLOR",0);

        if(scorecolor > highScorecolor){
            HighScoreLabel.setText("High Score : " + scorecolor);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_COLOR", scorecolor);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScorecolor);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
