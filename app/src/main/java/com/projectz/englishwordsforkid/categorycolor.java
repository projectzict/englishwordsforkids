package com.projectz.englishwordsforkid;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class categorycolor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorycolor);

        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Color");

        Button btnwhite = (Button)findViewById(R.id.btnwhite);
        Button btnpurple = (Button)findViewById(R.id.btnpurple);
        Button btnblue = (Button)findViewById(R.id.btnblue);
        Button btngreen = (Button)findViewById(R.id.btngreen);
        Button btnyellow = (Button)findViewById(R.id.btnyellow);
        Button btnorange = (Button)findViewById(R.id.btnorange);
        Button btnred = (Button)findViewById(R.id.btnred);
        Button btnpink = (Button)findViewById(R.id.btnpink);
        Button btnbrown = (Button)findViewById(R.id.btnbrown);
        Button btnblack = (Button)findViewById(R.id.btnblack);

        btnwhite.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,white.class));
            }
        });

        btnpurple.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,purple.class));
            }
        });

        btnblue.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,blue.class));
            }
        });

        btngreen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,green.class));
            }
        });

        btnyellow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,yellow.class));
            }
        });

        btnorange.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,orange.class));
            }
        });

        btnred.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,red.class));
            }
        });

        btnpink.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,pink.class));
            }
        });

        btnbrown.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,brown.class));
            }
        });

        btnblack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(categorycolor.this,black.class));
            }
        });
    }
}
