package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class resultnumbertest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultnumbertest);

        TextView resultLabel = (TextView) findViewById(R.id.resultday);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscoreday);

        int scorenumber = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_NUMBER", 0);
        resultLabel.setText(scorenumber+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_NUMBER",Context.MODE_PRIVATE);
        int highScoreNumber = settings.getInt("HIGH_SCORE_NUMBER",0);

        if(scorenumber > highScoreNumber){
            HighScoreLabel.setText("High Score : " + scorenumber);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_NUMBER", scorenumber);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScoreNumber);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
