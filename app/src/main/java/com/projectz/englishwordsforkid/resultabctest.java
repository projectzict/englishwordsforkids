package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class resultabctest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultabctest);

        TextView resultLabel = (TextView) findViewById(R.id.resultanimal);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscoreday);

        int scoreabc = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_ABC", 0);
        resultLabel.setText(scoreabc+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_ABC",Context.MODE_PRIVATE);
        int highScoreabc = settings.getInt("HIGH_SCORE",0);

        if(scoreabc > highScoreabc){
            HighScoreLabel.setText("High Score : " + scoreabc);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_ABC", scoreabc);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScoreabc);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
