package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class resultbodytest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultbodytest);
        TextView resultLabel = (TextView) findViewById(R.id.resultbody);
        TextView HighScoreLabel = (TextView) findViewById(R.id.highscorebody);

        int scorebody = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_BODY", 0);
        resultLabel.setText(scorebody+" / 10");

        SharedPreferences settings = getSharedPreferences("HIGH_SCORE_BODY",Context.MODE_PRIVATE);
        int highScorebody = settings.getInt("HIGH_SCORE_BODY",0);

        if(scorebody > highScorebody){
            HighScoreLabel.setText("High Score : " + scorebody);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE_BODY", scorebody);
            editor.commit();
        }else{
            HighScoreLabel.setText("High Score : " + highScorebody);
        }


    }
    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}