package com.projectz.englishwordsforkid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class score extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        TextView HighScoreday = (TextView) findViewById(R.id.highscoreday);
        TextView HighScoremonth = (TextView) findViewById(R.id.highscoremonth);
        TextView HighScoreanimal = (TextView) findViewById(R.id.highscoreanimal);
        TextView HighScoreobject = (TextView) findViewById(R.id.highscoreobject);
        TextView HighScorecolor = (TextView) findViewById(R.id.highscorecolor);
        TextView HighScoreabc = (TextView) findViewById(R.id.highscoreabc);
        TextView HighScore123 = (TextView) findViewById(R.id.highscore123);
        TextView HighScorefruit = (TextView) findViewById(R.id.highscorefruit);
        TextView HighScorebody = (TextView) findViewById(R.id.highscorebody);
        TextView HighScorevehicle = (TextView) findViewById(R.id.highscorevehicle);



        int scoreday = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_DAY" ,0);
        SharedPreferences settingsday = getSharedPreferences("HIGH_SCORE_DAY",Context.MODE_PRIVATE);
        int highScoreday = settingsday.getInt("HIGH_SCORE_DAY",0);
        if(scoreday > highScoreday){
            HighScoreday.setText("High Score : " + scoreday);
            SharedPreferences.Editor editor = settingsday.edit();
            editor.putInt("HIGH_SCORE_DAY", scoreday);
            editor.commit();
        }else{
            HighScoreday.setText("High Score : " + highScoreday + "/7");
        }

        int scoremonth = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_MONTH" ,0);
        SharedPreferences settingsmonth = getSharedPreferences("HIGH_SCORE_MONTH",Context.MODE_PRIVATE);
        int highScoremonth = settingsmonth.getInt("HIGH_SCORE_MONTH",0);
        if(scoremonth > highScoremonth){
            HighScoremonth.setText("High Score : " + scoremonth);
            SharedPreferences.Editor editor = settingsmonth.edit();
            editor.putInt("HIGH_SCORE_MONTH", scoremonth);
            editor.commit();
        }else{
            HighScoremonth.setText("High Score : " + highScoremonth + "/10");
        }

        int scoreanimal = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_ANIMAL" ,0);
        SharedPreferences settingsanimal = getSharedPreferences("HIGH_SCORE_ANIMAL",Context.MODE_PRIVATE);
        int highScoreanimal = settingsanimal.getInt("HIGH_SCORE_ANIMAL",0);
        if(scoreanimal > highScoreanimal){
            HighScoreanimal.setText("High Score : " + scoreanimal);
            SharedPreferences.Editor editor = settingsanimal.edit();
            editor.putInt("HIGH_SCORE_ANIMAL", scoreanimal);
            editor.commit();
        }else{
            HighScoreanimal.setText("High Score : " + highScoreanimal + "/10");
        }

        int scoreobject = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_OBJECT" ,0);
        SharedPreferences settingsobject = getSharedPreferences("HIGH_SCORE_OBJECT",Context.MODE_PRIVATE);
        int highScoreobject = settingsobject.getInt("HIGH_SCORE_OBJECT",0);
        if(scoreobject > highScoreobject){
            HighScoreobject.setText("High Score : " + scoreobject);
            SharedPreferences.Editor editor = settingsobject.edit();
            editor.putInt("HIGH_SCORE_OBJECT", scoreobject);
            editor.commit();
        }else{
            HighScoreobject.setText("High Score : " + highScoreobject + "/10");
        }

        int scorecolor = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_COLOR" ,0);
        SharedPreferences settingscolor = getSharedPreferences("HIGH_SCORE_COLOR",Context.MODE_PRIVATE);
        int highScorecolor = settingscolor.getInt("HIGH_SCORE_COLOR",0);
        if(scorecolor > highScorecolor){
            HighScorecolor.setText("High Score : " + scorecolor);
            SharedPreferences.Editor editor = settingscolor.edit();
            editor.putInt("HIGH_SCORE_COLOR", scorecolor);
            editor.commit();
        }else{
            HighScorecolor.setText("High Score : " + highScorecolor + "/10");
        }

        int scoreabc = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_ABC" ,0);
        SharedPreferences settingsabc = getSharedPreferences("HIGH_SCORE_ABC",Context.MODE_PRIVATE);
        int highScoreabc = settingsabc.getInt("HIGH_SCORE_ABC",0);
        if(scoreabc > highScoreabc){
            HighScoreabc.setText("High Score : " + scoreabc);
            SharedPreferences.Editor editor = settingsabc.edit();
            editor.putInt("HIGH_SCORE_ABC", scoreabc);
            editor.commit();
        }else{
            HighScoreabc.setText("High Score : " + highScoreabc + "/10");
        }

        int score123 = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_NUMBER" ,0);
        SharedPreferences settings123 = getSharedPreferences("HIGH_SCORE_NUMBER",Context.MODE_PRIVATE);
        int highScore123 = settings123.getInt("HIGH_SCORE_NUMBER",0);
        if(score123 > highScore123){
            HighScore123.setText("High Score : " + score123);
            SharedPreferences.Editor editor = settings123.edit();
            editor.putInt("HIGH_SCORE_NUMBER", score123);
            editor.commit();
        }else{
            HighScore123.setText("High Score : " + highScore123 + "/10");
        }

        int scorefruit = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_FRUIT" ,0);
        SharedPreferences settingsfruit = getSharedPreferences("HIGH_SCORE_FRUIT",Context.MODE_PRIVATE);
        int highScorefruit = settingsfruit.getInt("HIGH_SCORE_FRUIT",0);
        if(scorefruit > highScorefruit){
            HighScorefruit.setText("High Score : " + scorefruit);
            SharedPreferences.Editor editor = settingsfruit.edit();
            editor.putInt("HIGH_SCORE_FRUIT", scorefruit);
            editor.commit();
        }else{
            HighScorefruit.setText("High Score : " + highScorefruit + "/10");
        }

        int scorebody = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_BODY" ,0);
        SharedPreferences settingsbody = getSharedPreferences("HIGH_SCORE_BODY",Context.MODE_PRIVATE);
        int highScorebody = settingsbody.getInt("HIGH_SCORE_BODY",0);
        if(scorebody > highScorebody){
            HighScorebody.setText("High Score : " + scorebody);
            SharedPreferences.Editor editor = settingsbody.edit();
            editor.putInt("HIGH_SCORE_BODY", scorebody);
            editor.commit();
        }else{
            HighScorebody.setText("High Score : " + highScorebody + "/10");
        }

        int scorevehicle = getIntent().getIntExtra("RIGHT_ANSWER_COUNT_VEHICLE" ,0);
        SharedPreferences settingsvehicle = getSharedPreferences("HIGH_SCORE_VEHICLE",Context.MODE_PRIVATE);
        int highScorevehicle = settingsvehicle.getInt("HIGH_SCORE_VEHICLE",0);
        if(scorevehicle > highScorevehicle){
            HighScorevehicle.setText("High Score : " + scorevehicle);
            SharedPreferences.Editor editor = settingsvehicle.edit();
            editor.putInt("HIGH_SCORE_VEHICLE", scorevehicle);
            editor.commit();
        }else{
            HighScorevehicle.setText("High Score : " + highScorevehicle + "/10");
        }

    }

    public void returnTop (View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
